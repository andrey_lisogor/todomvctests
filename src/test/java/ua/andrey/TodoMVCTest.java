package ua.andrey;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.junit.Test;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

/**
 * Created by aalis_000 on 22.02.2016.
 */
public class TodoMVCTest extends AtToDoMVCTestWithClearedDataAfterEachTest {

    @Test
    public void testTasksLifeCycle() {

        add("1");

        startEdit("1", "1 edited").pressTab();
        assertItemsLeft(1);

        //complete
        toogle("1 edited");
        assertTasks("1 edited");

        filterCompleted();
        startEdit("1 edited", "1 cancel edit").pressEscape();

        //reopen
        toogle("1 edited");
        assertNoTasks();

        filterActive();
        assertTasks("1 edited");
        add("2");
        assertTasks("1 edited", "2");

        delete("1 edited");
        assertTasks("2");

        //complete all
        toogleAll();
        assertNoTasks();

        filterAll();
        assertTasks("2");
        clearCompleted();
        assertNoTasks();

    }

    @Test
    public void testDeleteByClearingText() {
        //given - added task on active filter
        add("1", "2");
        filterActive();

        startEdit("1", "").pressEnter();
        assertTasks("2");
        assertItemsLeft(1);
    }

    @Test
    public void testReopenAllAtCompleted() {
        //given - completed tasks on completed filter
        add("1", "2");
        toogleAll();
        filterCompleted();

        toogleAll();
        assertItemsLeft(2);
        assertNoTasks();
    }

    @Test
    public void comfirmEditByClickingOutside() {
        //given - added task on active filter
        add("1");
        filterActive();

        startEdit("1", "1 edited");
        newTask.click();
        assertItemsLeft(1);
        assertTasks("1 edited");
    }

    ElementsCollection tasks = $$("#todo-list li");

    SelenideElement newTask = $("#new-todo");

    @Step
    private void add(String... taskTexts) {
        for (String text : taskTexts) {
            newTask.setValue(text).pressEnter();
        }
    }

    @Step
    private SelenideElement startEdit(String oldTaskText, String newTaskText) {
        tasks.find(exactText(oldTaskText)).doubleClick();
        return tasks.find(cssClass("editing")).$(".edit").setValue(newTaskText);
    }

    @Step
    private void delete(String taskTexts) {
        tasks.find(exactText(taskTexts)).hover().$(".destroy").click();
    }

    @Step
    private void toogle(String taskTexts) {
        tasks.find(exactText(taskTexts)).$(".toggle").click();
    }

    @Step
    private void toogleAll() {
        $("#toggle-all").click();
    }

    @Step
    private void clearCompleted() {
        $("#clear-completed").click();
        $("#clear-completed").shouldBe(hidden);
    }

    @Step
    private void filterActive() {
        $(By.linkText("Active")).click();
    }

    @Step
    private void filterAll() {
        $(By.linkText("All")).click();
    }

    @Step
    private void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    @Step
    private void assertTasks(String... taskTexts) {
        tasks.filter(visible).shouldHave(exactTexts(taskTexts));
    }

    @Step
    private void assertNoTasks() {
        tasks.filter(visible).shouldBe(empty);
    }

    @Step
    private void assertItemsLeft(int count) {
        $("#todo-count strong").shouldHave(exactText(Integer.toString(count)));
    }


}
